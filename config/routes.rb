Rails.application.routes.draw do
  get 'home/index'

  constraints(subdomain: 'admin') do
    namespace :admin, path: '/' do
      get '/' => 'home#index' #Como esta dentro del namespace, la ruta de este home es. ´admin/home#index´
    end
  end

  root 'home#index'
end
